<?php

header('Access-Control-Allow-Origin: *');

function getDurationOfAudio($path) {
    $cmd = "ffmpeg -i " . escapeshellarg($path) . " 2>&1 | grep 'Duration' | cut -d ' ' -f 4 | sed s/,//";
    
    $time_stamp = exec($cmd);

    $time_pieces = explode(":", $time_stamp);
    $seconds_pieces = explode(".", $time_pieces[2]);
    
    $seconds = $time_pieces[0] * 3600 + $time_pieces[1] * 60 + $seconds_pieces[0];
    $miliseconds = $seconds_pieces[1];
    
    return("$seconds.$miliseconds");
}

function epicfail($msg) {
    $ar = array();
    $ar["status"] = "fail";
    $ar["msg"] = $msg;
    echo json_encode($ar);
    die();
}

function win($msg, $die = true) {
    $ar = array();
    $ar["status"] = "success";
    $ar["msg"] = $msg;
    echo json_encode($ar);
    if ($die) {
        die();
    } else {
        fastcgi_finish_request();
    }
}

function tryvar($var) {
    if (!isset($_GET[$var]) || $_GET[$var] == "") {
        epicfail($var." not set");
    }
}

function deltree($dir) {

    $files = array_diff(scandir($dir), array('.','..'));
 
    foreach ($files as $file) {
 
        (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
 
    }
 
    return rmdir($dir);
 
}

$rt = $_SERVER['DOCUMENT_ROOT'];

if (!isset($_GET["pw"]) || $_GET["pw"] != "mothmoon") {
    epicfail("wrong pw. L");
}
switch (@$_GET["action"]) {
    case "download":
        tryvar("url");
        tryvar("folder");
        $dl = "yt-dlp ";
        $dl .= "-o \"" . $rt . "/dl/".escapeshellcmd($_GET["folder"])."/%(uploader)s-%(title)s-%(id)s.%(ext)s\" ";
        $dl .= "-f ba -x ";
        $dl .= escapeshellarg($_GET["url"]);
        win("ok going", false);
        shell_exec($dl);
        break;
    case "files":
        tryvar("folder");
        $js = array();
        $glb = glob($rt . "/dl/".$_GET["folder"]."/*");
        usort($glb, function($a, $b) {
            return filectime($a) - filectime($b);
        });
        foreach ($glb as $dir) {
            $owo = explode("/", $dir);
            array_push($js, end($owo));
        }
        if (empty($js)) {
            epicfail("folder empty or nonexistant...");
        }
        win(json_encode($js));
        break;
    case "files_move":
        tryvar("oldfolder");
        tryvar("oldfile");
        tryvar("newfolder");
        tryvar("newfile");
        if (!file_exists($rt."/dl/".$_GET["oldfolder"]."/".$_GET["oldfile"])) {
            epicfail("oldfile doesnt exist!!");
        }
        if (rename($rt . "/dl/" . $_GET["oldfolder"] . "/" . $_GET["oldfile"], $rt . "/dl/" . $_GET["newfolder"] . "/" . $_GET["newfile"])) {
            win("/".$_GET["newfolder"] . "/" . $_GET["newfile"]);
        } else {
            epicfail("failed to move file");
        }
        break;
    case "files_delete":
        tryvar("folder");
        tryvar("file");
        if (!file_exists($rt."/dl/".$_GET["folder"]."/".$_GET["file"])) {
            epicfail("file doesnt exist!!");
        }
        if (unlink($rt."/dl/".$_GET["folder"]."/".$_GET["file"])) {
            win("gone!");
        } else {
            epicfail("fail");
        }
        break;
    case "folders":
        $js = array();
        foreach (glob($rt . "/dl/*", GLOB_ONLYDIR) as $dir) {
            $owo = explode("/", $dir);
            array_push($js, end($owo));
        }
        if (empty($js)) {
            epicfail("no folders...");
        }
        win(json_encode($js));
        break;
    case "folders_move":
        tryvar("oldfolder");
        tryvar("newfolder");
        if (!file_exists($rt."/dl/".$_GET["oldfolder"])) {
            epicfail("oldfolder doesnt exist!!");
        }
        if (file_exists($rt."/dl/".$_GET["newfolder"])) {
            epicfail("newfolder already exists!!");
        }
        if (rename($rt . "/dl/" . $_GET["oldfolder"], $rt . "/dl/" . $_GET["newfolder"])) {
            win("/".$_GET["newfolder"]);
        } else {
            epicfail("failed to move folder");
        }
        break;
    case "folders_delete":
        tryvar("folder");
        if (!file_exists($rt."/dl/".$_GET["folder"])) {
            epicfail("folder doesnt exist!!");
        }
        if (deltree($rt."/dl/".$_GET["folder"])) {
            win("gone!");
        } else {
            epicfail("failed to delete folder...");
        }
        break;
    default:
        win("nyaaaa~\n*curls up in your lap and purrs like a good lil trash kitty*");
        break;
}

?>
